<?xml version="1.0" encoding="UTF-8"?>
<simconf>
  <project EXPORT="discard">[APPS_DIR]/mrm</project>
  <project EXPORT="discard">[APPS_DIR]/mspsim</project>
  <project EXPORT="discard">[APPS_DIR]/avrora</project>
  <project EXPORT="discard">[APPS_DIR]/serial_socket</project>
  <project EXPORT="discard">[APPS_DIR]/collect-view</project>
  <project EXPORT="discard">[APPS_DIR]/powertracker</project>
  <simulation>
    <title>My simulation</title>
    <speedlimit>1.0</speedlimit>
    <randomseed>generated</randomseed>
    <motedelay_us>1000000</motedelay_us>
    <radiomedium>
      org.contikios.cooja.radiomediums.UDGM
      <transmitting_range>50.0</transmitting_range>
      <interference_range>100.0</interference_range>
      <success_ratio_tx>1.0</success_ratio_tx>
      <success_ratio_rx>1.0</success_ratio_rx>
    </radiomedium>
    <events>
      <logoutput>40000</logoutput>
    </events>
    <motetype>
      org.contikios.cooja.mspmote.Z1MoteType
      <identifier>z11</identifier>
      <description>Z1 Mote Type #z11</description>
      <source EXPORT="discard">[CONTIKI_DIR]/examples/ipv6/multicast/simulations-bmrf-simple-contikiMAC-real/root.c</source>
      <commands EXPORT="discard">make root.z1 TARGET=z1</commands>
      <firmware EXPORT="copy">[CONTIKI_DIR]/examples/ipv6/multicast/simulations-bmrf-simple-contikiMAC-real/root.z1</firmware>
      <moteinterface>org.contikios.cooja.interfaces.Position</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.IPAddress</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspButton</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspDefaultSerial</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspLED</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface>
    </motetype>
    <motetype>
      org.contikios.cooja.mspmote.Z1MoteType
      <identifier>z12</identifier>
      <description>Z1 Mote Type #z12</description>
      <source EXPORT="discard">[CONTIKI_DIR]/examples/ipv6/multicast/simulations-bmrf-simple-contikiMAC-real/sink.c</source>
      <commands EXPORT="discard">make sink.z1 TARGET=z1</commands>
      <firmware EXPORT="copy">[CONTIKI_DIR]/examples/ipv6/multicast/simulations-bmrf-simple-contikiMAC-real/sink.z1</firmware>
      <moteinterface>org.contikios.cooja.interfaces.Position</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.RimeAddress</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.IPAddress</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.Mote2MoteRelations</moteinterface>
      <moteinterface>org.contikios.cooja.interfaces.MoteAttributes</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspClock</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspMoteID</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspButton</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.Msp802154Radio</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspDefaultSerial</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspLED</moteinterface>
      <moteinterface>org.contikios.cooja.mspmote.interfaces.MspDebugOutput</moteinterface>
    </motetype>
    <mote>
      <breakpoints />
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>29.69545630651221</x>
        <y>77.38284442296614</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspClock
        <deviation>1.0</deviation>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspMoteID
        <id>1</id>
      </interface_config>
      <motetype_identifier>z11</motetype_identifier>
    </mote>
    <mote>
      <breakpoints />
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>30.329034312469286</x>
        <y>95.37556854460959</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspClock
        <deviation>1.0</deviation>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspMoteID
        <id>2</id>
      </interface_config>
      <motetype_identifier>z12</motetype_identifier>
    </mote>
    <mote>
      <breakpoints />
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>28.712261907695762</x>
        <y>112.15355213521599</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspClock
        <deviation>1.0</deviation>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspMoteID
        <id>3</id>
      </interface_config>
      <motetype_identifier>z12</motetype_identifier>
    </mote>
    <mote>
      <breakpoints />
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>30.84732995588287</x>
        <y>128.5048774466656</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspClock
        <deviation>1.0</deviation>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspMoteID
        <id>4</id>
      </interface_config>
      <motetype_identifier>z12</motetype_identifier>
    </mote>
    <mote>
      <breakpoints />
      <interface_config>
        org.contikios.cooja.interfaces.Position
        <x>30.578184446622597</x>
        <y>145.9271431069772</y>
        <z>0.0</z>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspClock
        <deviation>1.0</deviation>
      </interface_config>
      <interface_config>
        org.contikios.cooja.mspmote.interfaces.MspMoteID
        <id>5</id>
      </interface_config>
      <motetype_identifier>z12</motetype_identifier>
    </mote>
  </simulation>
  <plugin>
    org.contikios.cooja.plugins.SimControl
    <width>280</width>
    <z>1</z>
    <height>160</height>
    <location_x>400</location_x>
    <location_y>0</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.Visualizer
    <plugin_config>
      <moterelations>true</moterelations>
      <skin>org.contikios.cooja.plugins.skins.IDVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.GridVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.TrafficVisualizerSkin</skin>
      <skin>org.contikios.cooja.plugins.skins.UDGMVisualizerSkin</skin>
      <viewport>1.4663700314620083 0.0 0.0 1.4663700314620083 213.4360147780046 8.777524002915104</viewport>
    </plugin_config>
    <width>400</width>
    <z>3</z>
    <height>400</height>
    <location_x>1</location_x>
    <location_y>1</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.TimeLine
    <plugin_config>
      <mote>0</mote>
      <mote>1</mote>
      <mote>2</mote>
      <mote>3</mote>
      <mote>4</mote>
      <showRadioRXTX />
      <showRadioHW />
      <showLEDs />
      <zoomfactor>500.0</zoomfactor>
    </plugin_config>
    <width>1187</width>
    <z>5</z>
    <height>166</height>
    <location_x>0</location_x>
    <location_y>811</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.Notes
    <plugin_config>
      <notes>Enter notes here</notes>
      <decorations>true</decorations>
    </plugin_config>
    <width>507</width>
    <z>4</z>
    <height>160</height>
    <location_x>680</location_x>
    <location_y>0</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.ScriptRunner
    <plugin_config>
      <script>//import Java Package to JavaScript 
importPackage(java.io);
simulation_time = 785000; TIMEOUT(900000, timeout_func());
/* milliseconds */
outs = new Array(100);
total_time = 0;
total_ins = 0;
pdr = 0; 
frame_tx = 0; 
msg_tx = 0; 
rx_time = 0; 
tx_time = 0; 
lpm_time = 0; 
cpu_time = 0; 
total_subscribed = 0; 
total_nodes = 0;
//create files 
fr = new File("raw_results.csv"); 
fd = new File("delay_results.csv"); 
fs = new File("summary_results.csv"); 
fdup = new File("duplicates_results.csv"); 
frawdup = new File("duplicates_raw_results.csv");

 //Use this if using the GUI //
 f = new File("../../../examples/ipv6/multicast/simulations-bmrf/log.txt"); 
 tfraw = new FileWriter(fr); 
 tfdelay = new FileWriter(fd); 
 tfsummary = new FileWriter(fs); 
 tfduplicates = new FileWriter(fdup); 
 tfraw_duplicates = new FileWriter(frawdup); 
 timeout_func = function simulationEnd() { 
     /* Extract PowerTracker statistics */ 
     plugin = mote.getSimulation().getCooja().getStartedPlugin("PowerTracker"); 
    if (plugin != null) { 
         stats = plugin.radioStatistics(); 
         splited_stats = stats.split(/\r\n|\n|\r/); 
         splited_on = splited_stats[0].split(" "); 
         splited_tx = splited_stats[1].split(" "); 
         splited_rx = splited_stats[2].split(" "); 
         splited_int = splited_stats[3].split(" "); 
         idle_listening = (splited_on[2]-splited_tx[2]-splited_rx[2])/(1000000*51); 
        //
        //log.log("PowerTracker: Extracted statistics:\n" + "AVG ON " + splited_on[2]/(1000000*51) + "s " + splited_on[4] + "%" +"\n"+ "AVG TX " + splited_tx[2]/(1000000*51) + "s " + splited_tx[4] + "%" + "\n" 
        // + "AVG RX " + splited_rx[2]/(1000000*51) + "s " + splited_rx[4] + "%" + "\n" + "AVG IDLE LISTENING " + idle_listening + "s " + idle_listening*100/(simulation_time/1000) + "%" + "\n" + "AVG INT " + splited_int[2]/(1000000*51) + "s " 
        //+ splited_int[4] + "%" + "\n" + "\n"); 
        tfsummary.write("PowerTracker: Extracted statistics:\n" + "AVG ON " + splited_on[2]/(1000000*51) + "s " + splited_on[4] + "%" +"\n" + "AVG TX " + splited_tx[2]/(1000000*51) + "s " 
                        + splited_tx[4] + "%" + "\n" + "AVG RX " + splited_rx[2]/(1000000*51) + "s " 
                        + splited_rx[4] + "%" + "\n" + "AVG IDLE LISTENING " + idle_listening + "s " 
                        + idle_listening*100/(simulation_time/1000) + "%" + "\n" + "AVG INT " 
                        + splited_int[2]/(1000000*51) + "s " + splited_int[4] + "%" + "\n" + "\n");
    } 
    else { 
        //log.log("No PowerTracker plugin\n");
         tfsummary.write("No PowerTracker plugin\n");
    } 
    //log.log("Average end-to-end delay: " + (total_time/total_ins) +"\n"); 
    //log.log("Packet delivery ratio: " + (pdr/total_subscribed) + "\n"); 
    //log.log("Frame transmission: " + (frame_tx/total_nodes) + "\n"); 
    //log.log("Packet transmission: " + (msg_tx/total_nodes) + "\n"); 
    //log.log("Rx time: " + (rx_time/total_nodes) + " = " + ((rx_time/total_nodes)/32768) + " s" + "\n"); 
    //log.log("Tx time: " + (tx_time/total_nodes) + " = " + ((tx_time/total_nodes)/32768) + " s" + "\n"); 
    //log.log("LPM time: " + (lpm_time/total_nodes) + " = " + ((lpm_time/total_nodes)/32768) + " s" + "\n"); 
    //log.log("CPU time: " + (cpu_time/total_nodes) + " = " + ((cpu_time/total_nodes)/32768) + " s" + "\n"); 
    tfsummary.write("Average end-to-end delay: " + (total_time/total_ins) +"\n"); 
    tfsummary.write("Packet delivery ratio: " + (pdr/total_subscribed) + "\n"); 
    tfsummary.write("Frame transmission: " + (frame_tx/total_nodes) + "\n"); 
    tfsummary.write("Packet transmission: " + (msg_tx/total_nodes) + "\n"); 
    tfsummary.write("Rx time: " + (rx_time/total_nodes) + " = " + ((rx_time/total_nodes)/32768) + " s" + "\n"); 
    tfsummary.write("Tx time: " + (tx_time/total_nodes) + " = " + ((tx_time/total_nodes)/32768) + " s" + "\n"); 
    tfsummary.write("LPM time: " + (lpm_time/total_nodes) + " = " + ((lpm_time/total_nodes)/32768) + " s" + "\n"); 
    tfsummary.write("CPU time: " + (cpu_time/total_nodes) + " = " + ((cpu_time/total_nodes)/32768) + " s" + "\n"); 
    tfraw.close(); 
    tfdelay.close(); 
    tfsummary.close(); 
    tfduplicates.close(); 
    tfraw_duplicates.close(); 
    log.testOK(); 
} 
while(true){ 
    YIELD(); 
    time_msg = sim.getSimulationTimeMillis();
    time_msg_seconds = time_msg/1000; 
    message = msg.split(";");  //dit maakt een array van de string dus we moeten kijken hoelang onze array is om te weten als er waardevolle data aanwezig is
    //log.log(""+message[0]+"\n"); 
    if (message.length == 3) { 
        if (message[0]=="Out") { 
            outs[parseInt(message[1])] = time_msg; 
            //log.log("New out time\n"); 
            //log.log("outs["+parseInt(message[1])+"] = "+outs[parseInt(message[1])]+" = "+time_msg+"\n");
        } 
        else if (message[0]=="In") { 
            total_time = total_time + (time_msg - outs[parseInt(message[1])]); 
            total_ins++; 
            //log.log("Delay: "+time_msg+" + "+outs[parseInt(message[1])]+" = "+(time_msg - outs[parseInt(message[1])])+"\n"); 
            //log.log(""+parseInt(message[1])+";"+id+";"+(time_msg - outs[parseInt(message[1])])+"\n"); 
            tfdelay.write(""+parseInt(message[1])+";"+id+";"+(time_msg - outs[parseInt(message[1])])+ ";" + parseInt(message[2]) + "\n"); 
        }
        else if (message[0]=="Duplicates") { 
            tfduplicates.write(""+id+";"+parseInt(message[1])+"\n"); 
        }
        else if (message[0]=="Received duplicate") { 
            tfraw_duplicates.write((time_msg_seconds/60|0) + ":" + (time_msg_seconds % 60).toFixed(3) + ";ID:" + id + ";" + msg + "\n"); 
        } 
    } 
    else if (message.length == 8) { 
        if (message[0] != "n") { 
            pdr += parseInt(message[0]); total_subscribed++; 
        } 
        frame_tx += parseInt(message[1]); 
        msg_tx += parseInt(message[2]); 
        rx_time += parseInt(message[3]); 
        tx_time += parseInt(message[4]); 
        lpm_time += parseInt(message[5]); 
        cpu_time += parseInt(message[6]); 
        total_nodes++; 
        tfraw.write((time_msg_seconds/60|0) + ":" + (time_msg_seconds % 60).toFixed(3) + ";ID:" + id + ";" + id + ";" + msg + "\n"); 
    } 
}</script>
      <active>false</active>
    </plugin_config>
    <width>600</width>
    <z>2</z>
    <height>700</height>
    <location_x>1229</location_x>
    <location_y>52</location_y>
  </plugin>
  <plugin>
    org.contikios.cooja.plugins.LogListener
    <plugin_config>
      <filter />
      <formatted_time />
      <coloring />
    </plugin_config>
    <width>785</width>
    <z>0</z>
    <height>396</height>
    <location_x>400</location_x>
    <location_y>160</location_y>
  </plugin>
</simconf>


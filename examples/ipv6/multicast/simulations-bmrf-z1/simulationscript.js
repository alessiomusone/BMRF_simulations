importPackage(java.io);

var output_array = [];


//join multicast group
mote2 = sim.getMoteWithID(2);
mote2.getInterfaces().getButton().clickButton()

//start sending
mote1 = sim.getMoteWithID(1);
mote1.getInterfaces().getButton().clickButton()

var line = "";
writer = new FileWriter("tesst.txt");

while (true) {
    var message = msg;
    var n = message.startsWith("Out;");
    var p = message.startsWith("In;");
    var end = message.startsWith("Duplicates");

    if(n || p){
        line = time + "," + id + "," + msg;
        output_array.push(line);
        writer.writer(line + "\n");
        log.log(time + ":" + id + ":" + msg + "\n");
        //writer.write(msg + "\n");
    }
    try{
        YIELD();
    }
    catch (e) {
        //Close file.
            writer.close();
        //Rethrow exception again, to end the script.
        throw('test script killed');
    }
}